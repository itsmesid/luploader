<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

use function PHPSTORM_META\map;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('filelist', function (Request $request) {

    $dir = 'public';
    $list = [];
    $dlist = [];
    $files = Storage::files($dir);

    foreach ($files as $file) {
        $list[]= [
            'icon' => '',
            'text' => str_replace($dir,'',$file)
        ];
    }
    
    $directories = Storage::directories($dir);


    foreach ($directories as $directory) {
        $dlist[]= [
            'iconCls' => '',
            'cls' => '',
            'text' =>str_replace($dir,'',$directory)
        ];
    }

   
    return [
        'files'=> $list,
        'directories'=>$dlist
    ];
});
