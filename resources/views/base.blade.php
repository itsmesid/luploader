<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>File Manager Template </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">

        <div class="page-header">
       
 <h1>  <a href="/"><i class="fa fa-home" aria-hidden="true"></i></a> File Manager </h1> 
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-error">
                    {{ session('error') }}
                </div>
            @endif
        </div>

        <!-- File Manager - START -->

        <div class="container pb-filemng-template">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <nav class="navbar navbar-default pb-filemng-navbar">
                        <div class="container-fluid">
                            <!-- Navigation -->
                            <div class="navbar-header">
                                <form action="/search" method="GET" role="search">
                                    <div class="input-group" style="padding-top: 19px;padding-right: 20px;">
                                        <input type="text" class="form-control" name="q" value="{{(isset($q)? $q: '') }}" placeholder="Search users"> <span class="input-group-btn">
                                            <button type="submit" class="btn btn-default">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>

                            <ul class="collapse navbar-collapse nav navbar-nav navbar-right" id="options">
                                <form id="form"  action="/" method="POST" enctype="multipart/form-data">
                                    {{ method_field('PUT') }}
                                    {{ csrf_field() }}
                                    <div class="input-group" style="padding-top: 19px;padding-right: 23px;">
                                    <input type="file" name="upfile" onchange="form.submit()" />
                                    </div>
                                </form>
                            </ul>


                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="pb-filemng-navigation">
                                <ul class="nav navbar-nav">
                                    {{ $files->appends(request()->query())->links() }}
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->

                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                    <div class="panel panel-default">
                        <div class="panel-body pb-filemng-panel-body">
                            <div class="row">

                                <div class="col-sm-12 col-md-12 pb-filemng-template-body">


                                    @foreach ($files as $file)
                                    <div class="col-xs-6 col-sm-6 col-md-3 pb-filemng-body-folders"> <img title="{{$file->filename}} :{{$file->size}} Bytes" class="img-responsive" src="/icons8-file-64.png"><br /><br />
                                        <p class="pb-filemng-paragraphs"> <a href="/download/{{$file->id}}">{{$file->filename}} </a> (<a title="Delete" href="/delete/{{$file->id}}"> x </a>) </p>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .pb-filemng-template {
                margin-top: 40px;
                font-family: 'Sansita', sans-serif;
            }

            .pb-filemng-navbar {
                margin-bottom: 0;
            }

            .treeview-toggle-btn {
                margin-left: 15px;
            }

            .pb-filemng-template-btn {
                background-color: Transparent;
                background-repeat: no-repeat;
                border: none;
                cursor: pointer;
                outline: none;
                color: gray;
                padding: 0px 13px 0px 13px;
            }

            .pb-filemng-active {
                border-bottom: 2px solid #6d97db;
                color: #5f6977;
            }

            .pb-filemng-template-btn:hover {
                color: blue;
            }

            .pb-filemng-body-folders>img:hover {
                cursor: pointer;
            }

            .btn-align {
                margin-top: 7px;
            }

            .pb-filemng-template-treeview {
                border-right: 1px solid gray;
            }

            .pb-filemng-folder {
                color: orange;
                padding-bottom: 3px;
            }

            .pb-filemng-paragraphs {
                margin-top: -20px;
                text-align: center;
            }

            .img-responsive {
                margin: 0 auto;
            }

            @media screen and (max-width: 767px) {

                .pb-filemng-template-treeview {
                    border-right: none;
                }

                #options {
                    text-align: center;
                }

                #options>li {
                    display: inline-block;
                }

                #pb-filemng-navigation>ul {
                    text-align: center;
                }

                #pb-filemng-navigation>ul>li {
                    display: inline-block;
                }

            }
        </style>

        <!-- you need to include the shieldui css and js assets in order for the charts to work -->
        <link rel="stylesheet" type="text/css" href="https://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
        <script type="text/javascript" src="https://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
        <!-- <script type="text/javascript" src="https://prepbootstrap.com/Content/data/fileManagerData.js"></script> -->


        </script>
        <!-- File Manager - END -->

    </div>

</body>

</html>