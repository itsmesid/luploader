<?php

namespace App\Http\Controllers;

use App\Files;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class Upload extends Controller
{
    public function listFiles()
    {
        $files = Files::paginate(10);

        return view('base', ['files' => $files]);
    }

    public function search(Request $request)
    {
        $q = str_replace("*", "", $request->q);

        $files = Files::where(
            'filename',
            'LIKE',
            '%' . $q . '%'
        )->paginate(10)->appends(request()->query());;

        return view('base', ['files' => $files, 'q' => $request->q]);
    }


    public function uploadFile(Request $request)
    {
        $val = Validator::make($request->all(), [
            'upfile' => 'required|mimes:txt,doc,docx,pdf,png,jpeg,jpg,gif',
        ]);

        if ($val->fails()) {
            return redirect()->back()->with(['error' => 'No file received']);
        }

        // dd($request);


        $path = $request->file('upfile')->store('/');
        $file = new Files();
        $file->filename = $request->upfile->getClientOriginalName();
        $file->mime_type = $request->upfile->getMimeType();
        $file->size = $request->upfile->getSize();
        $file->storage_path = $path;
        $file->save();

        return redirect()->back()->with(['message' => 'File uploaded succesfully']);
        # code...
    }

    public function rename(Request $request)
    {
        $file = Files::find($request->id);

        if ($file === null) {
            return redirect()->back()->with(['error' => 'File not found']);
        }

        $file->filename = $request->filename;

        $file->save();

        return redirect()->back()->with(['message' => 'File renamed']);
    }

    public function delete($id)
    {
        $file = Files::find($id);

        if ($file === null) {
            return redirect()->back()->with(['error' => 'File not found']);
        }

        try {
            Storage::delete($file->file_path);
            $file->delete();
            return redirect()->back()->with(['message' => 'File Deleted']);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['error' => 'Unable to delete file']);
        }
    }

    public function download($id)
    {

        $file = Files::find($id);

        // return json_encode($file);

        if ($file === null) {
            exit('Requested file does not exist on our server!');
        }
        // Check if file exists in app/storage/file folder
        $file_path = storage_path() . "/app/" . $file->storage_path;
        // dd($file_path);
        $headers = array(
            'Content-Type' => $file->mime_type,
            'Content-Disposition' => 'attachment; filename=' . $file->filename,
        );

        if (file_exists($file_path)) {
            // Send Download
            return \Response::download($file_path, $file->filename, $headers);
        } else {
            // Error
            exit('Requested file does not exist on our server!');
        }
    }
}
